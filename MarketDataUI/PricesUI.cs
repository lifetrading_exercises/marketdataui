﻿using System;
using System.Windows.Forms;

namespace MarketDataUI
{
    public partial class PricesUi : Form
    {
        public PricesUi()
        {
            InitializeComponent();

            PricesDataGridView.AutoGenerateColumns = true;
            PricesDataGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            PricesDataGridView.RowHeadersVisible = false;
        }

        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        /// <summary>
        /// Start the flow of price data to the client
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void StartToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// Stop the flow of price data to the client
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void StopToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }
    }
}
